# CS-E4640 - DataFrame
Solution for the DataFrame assignment of CS-E4640 Big Data Platforms taught at Aalto University during the fall semester of 2018.

The assignment regards using DataFrames to process large datasets in Apache Spark.